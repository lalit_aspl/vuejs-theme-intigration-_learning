import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import UserComponent from '@/components/UserComponent'
import UserlistComponent from '@/components/UserlistComponent'

Vue.use(Router)

export default new Router({
  mode: 'history',
  hash: false,
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/user',
      name: 'UserComponent',
      component: UserComponent
    },

    {
      path: '/index',
      name: 'UserlistComponent',
      component: UserlistComponent
    }
  ]
})
